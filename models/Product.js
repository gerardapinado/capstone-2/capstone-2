const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, `Product name is required`]
    },
    description: {
        type: String,
        required: [true, `Product description is required`]
    },
    price: {
        type: Number,
        required: [true, `Product price is required`]
    },
    size: {
        type: String,
        required: [true, `Product price is required`]
    },
    img: {
        type: String,
        default: "https://yt3.ggpht.com/ytc/AKedOLQ6O8tijLXOiigpRae8KGHJJxK_vcXYCW8j9x31=s900-c-k-c0x00ffffff-no-rj"
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Product", productSchema)