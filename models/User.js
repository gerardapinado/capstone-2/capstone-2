// import mongoose library
const mongoose = require('mongoose')

// Create User Schema
const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, `Email is required`]
    },
    password: {
        type: String,
        required: [true, `Password is required`]
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
// timestamps: true as 2nd parameter of userSchema
}, {timestamps: true})

// Create and export User Model
module.exports = mongoose.model("User", userSchema)