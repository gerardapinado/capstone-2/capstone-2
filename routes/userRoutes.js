// import libraries
const express = require('express')
// declare router
const router = express.Router()

// import controller functions
const userCtrl = require('../controllers/userCtrl')

// import auth middleware
const { verify, verifyAdmin, decode } = require('../middleware/auth')

// Routing Starts here

// REGISTER User
router.post('/register', async (req,res) => {
    try{
        await userCtrl.registerUser(req.body).then(result => res.status(201).send(result))
    }catch(err) {
        res.status(500).json(err)
    }
})

// GET All User
router.get('/', async (req,res) => {
    try{
        await userCtrl.getAllUsers().then(result => res.status(200).send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// GET User
router.get('/profile', verify, async(req,res) => {
    
    const userId = decode(req.headers.authorization).id
    // console.log(userId)

    try{
        await userCtrl.getUser(userId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// SIGNIN User
router.post('/signin', async (req,res) => {
    try{
        await userCtrl.signInUser(req.body).then(result => res.status(200).send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// SET as Admin User
router.put('/:userId/setAsAdmin', verifyAdmin, async (req,res) => {
    // console.log(req.params.userId)
    console.log()
    try{
        await userCtrl.setAsAdmin(req.params.userId).then(result => res.send(result))
    }catch(err) {
        res.status(500).json(err)
    }
})




// Routing Ends here
// Export router
module.exports = router